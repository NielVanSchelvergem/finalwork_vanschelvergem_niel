﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class hobbyChoiseSystem : MonoBehaviour
{
    public static GameObject pointer;
    private GameObject pointerRaycastPoint;

    private int nextScene;
    // Start is called before the first frame update
    void Start()
    {
         pointer = gameObject.transform.GetChild(0).gameObject;
        pointerRaycastPoint =  pointer.transform.GetChild(0).gameObject;
        nextScene = SceneManager.GetActiveScene().buildIndex +1;
    }

    // Update is called once per frame
    void Update()
    {
       RaycastHit hit;

        if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,100)){
if (hit.collider.name.Contains("tekenCollider")){
    
SceneManager.LoadScene(nextScene);
      }

        }   
    }
}
