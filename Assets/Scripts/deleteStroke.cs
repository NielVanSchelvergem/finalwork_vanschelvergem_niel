﻿
/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deleteStroke : MonoBehaviour
{
     public  GameObject pointer;
    public GameObject pointerRaycastPoint;
    public GameObject drawing;
  
   
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
         if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,100)){
            Debug.DrawRay(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward*1000f, Color.magenta);
            if(hit.collider.name.Contains("deleteStrokeCollider")){
               
                
                Destroy(drawing.transform.GetChild(transform.childCount-1).gameObject);
            
                
            }
            }
    }
}
