﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class triangle : MonoBehaviour
{
    Mesh mesh;
    MeshRenderer meshRenderer;
    public Material material;
    Vector3[] vertices;
    int[] triangles;
    // Start is called before the first frame update
    void Start()  //We creëren een driehoek door 3 vertices te tekenen
    {
        meshRenderer= gameObject.AddComponent<MeshRenderer>();
        gameObject.AddComponent<MeshFilter>();
        
        gameObject.AddComponent<BoxCollider>();

        meshRenderer.material = material;

        mesh= new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        vertices = new[] {
        new Vector3(0,0,0),
        new Vector3(0,1,0),
        new Vector3(1,0,0),
        };
        mesh.vertices = vertices;
        triangles = new[] {0,1,2};
        mesh.triangles = triangles;

       string localPath = "Assets/Prefabs/" + gameObject.name + ".prefab";
       localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

        PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject,localPath,InteractionMode.UserAction);
    }

    // Update is called once per frame
   
}
