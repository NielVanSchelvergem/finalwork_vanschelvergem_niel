﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canvasPaintSystem : MonoBehaviour
{
     public  GameObject pointer;
    public GameObject pointerRaycastPoint;
  
  public GameObject trailPrefab;
  public GameObject pencilPrefab;
  GameObject thisTrail;
  Vector3 startPos;

  public Plane tekenCanvas;
   
    public GameObject Camera;
    public GameObject Player;
    public GameObject drawing;
    private int strokecounter;
    private bool enalbleTekenen;
    private GameObject lastStroke;

    public Material[] colors = new Material[8];
        public Material[] pencilColors = new Material[7];

    public Material[] textures = new Material[12];
  
    public GameObject brushSelected;
    public GameObject brushDeselected;
    public GameObject penSelected;
    public GameObject penDeselected;
    public GameObject pencilSelected;
    public GameObject pencilDeselected;

        public GameObject rulerSelected;
    public GameObject rulerDeselected;
            public GameObject rollerSelected;
    public GameObject rollerDeselected;
      


      
        public GameObject ColorsUi;
        public GameObject ToolsUi;
        public GameObject settings;
        public GameObject selections;
      
        public GameObject Textures;
        
        public Texture pencilTexture;
        private int drawingcount;
private TrailRenderer tr;

    // Start is called before the first frame update
    void Start()
    {
         tr = trailPrefab.GetComponent<TrailRenderer>();
       
       strokecounter = 2;
       enalbleTekenen = false;
         tr.minVertexDistance = 0.1f;
          tr.material.SetTextureScale("_MainTex", new Vector2(5.0f,5.0f));
    }

    // Update is called once per frame
    void Update()
    {
      drawingcount =  drawing.transform.childCount-1; 
                 RaycastHit hit;
                                                                          // Tekenen met meerde lijnen logica 
         if (Input.GetKeyDown(KeyCode.Space) && enalbleTekenen == false) //spatie op te beginnen met tekenen
        {
            if(pencilSelected.activeSelf){
               thisTrail = (GameObject)Instantiate(pencilPrefab, this.transform.position, Quaternion.identity);
           
           thisTrail.transform.SetParent(Camera.transform);
        
           enalbleTekenen = !enalbleTekenen;
                                                                                                                            //Wij soorten trail renderes, 1 gewone, 1 voor potlood met andere shader
            } else {
                thisTrail = (GameObject)Instantiate(trailPrefab, this.transform.position, Quaternion.identity);
           
           thisTrail.transform.SetParent(Camera.transform);
        
           enalbleTekenen = !enalbleTekenen;
            }
         
          
           
        } else

        if(Input.GetKeyDown(KeyCode.Space) && enalbleTekenen == true){ //terug spatie om te stoppen
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).SetParent(drawing.transform);         //Veranderen van de parent zodat het tekenobject de camera niet meer volgt en zo stopt met tekenen
                       enalbleTekenen = !enalbleTekenen;
        }
        

      if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,1000)){ //Raycast loodrecht op de pointer
    
     Debug.DrawRay(transform.position, pointerRaycastPoint.transform.forward, Color.green);


     if(hit.collider.name.Contains("deleteStrokeCollider")){   //stroke verwijderen
               
                
                Destroy(drawing.transform.GetChild(drawingcount).gameObject);
            
                
            }
           //Selecteren tools
     if(hit.collider.name.Contains("brushDeselected")){
       brushDeselected.SetActive(false);
       brushSelected.SetActive(true);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
        rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
      tr.startWidth = 40;
      tr.endWidth = 40;
      pointerRaycastPoint.transform.localScale = new Vector3(10f, 10f, 10f);
      tr.minVertexDistance = 0.1f;
     }

      if(hit.collider.name.Contains("rollerDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
       rollerDeselected.SetActive(false);
       rollerSelected.SetActive(true);
      tr.startWidth = 80;
      tr.endWidth = 80;
      pointerRaycastPoint.transform.localScale = new Vector3(20f, 20f, 20f);
      tr.minVertexDistance = 0.1f;
     }

        if(hit.collider.name.Contains("penDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(false);
       penSelected.SetActive(true);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
          rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
         tr.startWidth = 1;
      tr.endWidth = 1;
      pointerRaycastPoint.transform.localScale = new Vector3(1f, 1f, 1f);
      tr.minVertexDistance = 0.1f;
     }


   if(hit.collider.name.Contains("pencilDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(true);
       pencilDeselected.SetActive(false);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
          rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
        tr.startWidth = 3;
     tr.endWidth = 3;
      pointerRaycastPoint.transform.localScale = new Vector3(3f, 3f, 3f);
      tr.minVertexDistance = 0.1f;
      //trailPrefab.GetComponent<Renderer>().material= colors[0];
     
      
   

     }

   if(hit.collider.name.Contains("rulerDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
       rulerDeselected.SetActive(false);
       rulerSelected.SetActive(true);
          rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
         tr.startWidth = 5;
      tr.endWidth = 5;
      pointerRaycastPoint.transform.localScale = new Vector3(5f, 5f, 5f);
      tr.minVertexDistance = 100;
     }

     //////////////////////Colors/////////////////

      if(hit.collider.name.Contains("redColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[0];
         pencilPrefab.GetComponent<Renderer>().material = pencilColors[0];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[0];
            }

      if(hit.collider.name.Contains("orangeColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[1];
         pencilPrefab.GetComponent<Renderer>().material = pencilColors[1];

         pointerRaycastPoint.GetComponent<Renderer>().material = colors[1];
     }

       if(hit.collider.name.Contains("yellowColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[2];
         pencilPrefab.GetComponent<Renderer>().material = pencilColors[2];
         pointerRaycastPoint.GetComponent<Renderer>().material = colors[2];
         
     }

      if(hit.collider.name.Contains("greenColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[3];
         pencilPrefab.GetComponent<Renderer>().material = pencilColors[3];
         pointerRaycastPoint.GetComponent<Renderer>().material = colors[3];
     }

       if(hit.collider.name.Contains("blueColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[4];
         pencilPrefab.GetComponent<Renderer>().material = pencilColors[4];
         pointerRaycastPoint.GetComponent<Renderer>().material = colors[4];
         
     }

      if(hit.collider.name.Contains("purpleColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[5];
         pointerRaycastPoint.GetComponent<Renderer>().material =colors[5];
     }
           if(hit.collider.name.Contains("blackColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[6];
         pointerRaycastPoint.GetComponent<Renderer>().material =colors[6];
     }
       if(hit.collider.name.Contains("whiteColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[7];
         pointerRaycastPoint.GetComponent<Renderer>().material =colors[7];
     }
     ////////////////////////////Textures///////////////////////////
     if(hit.collider.name.Contains("Ice1")){
         trailPrefab.GetComponent<Renderer>().material = textures[0];

       setTexturePref();
     }

      if(hit.collider.name.Contains("Ice2")){
         trailPrefab.GetComponent<Renderer>().material = textures[1];
setTexturePref();
     }

    if(hit.collider.name.Contains("flowerField")){
         trailPrefab.GetComponent<Renderer>().material = textures[2];

setTexturePref();
     }
 if(hit.collider.name.Contains("Grass")){
         trailPrefab.GetComponent<Renderer>().material = textures[3];

      setTexturePref();
     }
 if(hit.collider.name.Contains("sandRock")){
         trailPrefab.GetComponent<Renderer>().material = textures[4];
setTexturePref();
     }
      if(hit.collider.name.Contains("redRock")){
         trailPrefab.GetComponent<Renderer>().material = textures[5];
setTexturePref();
         
     }
 if(hit.collider.name.Contains("asphalt1")){
         trailPrefab.GetComponent<Renderer>().material = textures[6];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("asphalt2")){
         trailPrefab.GetComponent<Renderer>().material = textures[7];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("asphalt3")){
         trailPrefab.GetComponent<Renderer>().material = textures[8];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick1")){
         trailPrefab.GetComponent<Renderer>().material = textures[9];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick2")){
         trailPrefab.GetComponent<Renderer>().material = textures[10];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick3")){
         trailPrefab.GetComponent<Renderer>().material = textures[11];
setTexturePref();
         
     }

//////////////////////// Menu's//////////////

      if(hit.collider.name.Contains("colorSelection")){
     ColorsUi.SetActive(true);
   Textures.SetActive(false);
   
     selections.SetActive(false);
            }

            if(hit.collider.name.Contains("natureSelection")){
              Textures.SetActive(true);
     ColorsUi.SetActive(false);
   ToolsUi.SetActive(false);
    selections.SetActive(false);          }


            if(hit.collider.name.Contains("settingSelection")){
     ColorsUi.SetActive(false);
   Textures.SetActive(false);
selections.SetActive(false);
ToolsUi.SetActive(false);
settings.SetActive(true);
            }

             if(hit.collider.name.Contains("backCol")){
     ColorsUi.SetActive(false);
  selections.SetActive(true);
     ToolsUi.SetActive(true);
            }

                if(hit.collider.name.Contains("backNat")){
     Textures.SetActive(false);
    selections.SetActive(true);
     ToolsUi.SetActive(true);
            }
    if(hit.collider.name.Contains("backSet")){
    settings.SetActive(false);
    selections.SetActive(true);
     ToolsUi.SetActive(true);
            }

  
        }  
    }

    //Instellingen van het tekenobject met trail renderer goedzetten voor kleuren en textures
    public void setColorPref(){
  tr.textureMode = LineTextureMode.Stretch;
}
    public void setTexturePref(){
tr.minVertexDistance = 100f;
         tr.numCornerVertices = 10;
              tr.startWidth = 100;
      tr.endWidth = 100;
      tr.textureMode = LineTextureMode.RepeatPerSegment;
    }
}
