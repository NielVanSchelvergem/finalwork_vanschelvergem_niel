﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBgSystem : MonoBehaviour
{
    // Start is called before the first frame update
      public GameObject pointer;
    public GameObject pointerRaycastPoint;

     public Material[] myMaterials = new Material[5];
     public GameObject background;
     public GameObject bgMenu;
 
    public AudioClip[] sounds;
    public AudioSource source;
     
     public GameObject paintmodeMenu;
    void Start()
    {
     source.loop = true;   
    }

    // Update is called once per frame
    void Update()
    {
            RaycastHit hit;

        if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,100)){

         
           
            if(hit.collider.name.Contains("changeBgCollider")){  
          bgMenu.SetActive(true);
          paintmodeMenu.SetActive(false);
            }
              if(hit.collider.name.Contains("backBg")){
          bgMenu.SetActive(false);
            }
            if(hit.collider.name.Contains("bg1")){
                background.GetComponent<Renderer>().material = myMaterials[0];
                source.GetComponent<AudioSource>().clip = sounds[0];
                source.GetComponent<AudioSource>().Play();
            }
             if(hit.collider.name.Contains("bg2")){
                background.GetComponent<Renderer>().material = myMaterials[1];
                    source.GetComponent<AudioSource>().clip = sounds[1];
                source.GetComponent<AudioSource>().Play();
                    }
             if(hit.collider.name.Contains("bg3")){
                background.GetComponent<Renderer>().material = myMaterials[2];
               source.GetComponent<AudioSource>().clip = sounds[2];
                source.GetComponent<AudioSource>().Play();
             }
             if(hit.collider.name.Contains("bg4")){
                background.GetComponent<Renderer>().material = myMaterials[3];

                   source.GetComponent<AudioSource>().clip = sounds[3];
                source.GetComponent<AudioSource>().Play();
            }
             if(hit.collider.name.Contains("bg5")){
                background.GetComponent<Renderer>().material = myMaterials[4];
                     source.GetComponent<AudioSource>().clip = sounds[3];
                source.GetComponent<AudioSource>().Play();
            }

            
        }
    

        
    }
}
