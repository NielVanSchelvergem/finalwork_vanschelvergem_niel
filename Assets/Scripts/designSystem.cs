﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class designSystem : MonoBehaviour
{
      public  GameObject pointer;
      public  GameObject cubePointer;
      public  GameObject circlePointer;
      public  GameObject trianglePointer;
      public GameObject text3Pointer;
        public GameObject text2Pointer;
          public GameObject text1Pointer;
    public GameObject pointerRaycastPoint;
  
  public GameObject trailPrefab;
  public GameObject cubePrefab;
  public GameObject circlePrefab;
  public GameObject trianglePrefab;

  public GameObject text3Prefab;
  public GameObject text2Prefab;
  public GameObject text1Prefab;

  public Plane tekenCanvas;
   
    public GameObject Camera;
    public GameObject Player;
    public GameObject drawing;
    

    public Material[] colors = new Material[7];
    

    public Material[] textures = new Material[12];

    
    public GameObject[] canvases = new GameObject[6];
     public GameObject[] canvasBackgrounds = new GameObject[5];
     public Material[] Backgrounds = new Material[13];
  
    public GameObject shapesSelected;
    public GameObject shapesDeselected;
    public GameObject penSelected;
    public GameObject penDeselected;
    public GameObject textSelected;
    public GameObject textDeselected;

        public GameObject rulerSelected;
    public GameObject rulerDeselected;
            public GameObject gridSelected;
    public GameObject gridDeselected;
      


      
        public GameObject ColorsUi;
        public GameObject Textures;
        public GameObject ToolsUi;
        public GameObject settings;
        public GameObject selections;
      public GameObject canvasMenu;
      public GameObject textMenu;
      public GameObject bgcanvases;
        public GameObject designCanvas;
        public GameObject paintModeMenu;
        public GameObject shapesMenu;
        public GameObject[] grids;
        public Camera[] cameras;
        private Vector3 upscaleX,upscaleY,dwonscaleX,downscaleY;
        private int drawingcount;
        private int strokecounter;
    private bool enalbleTekenen;
    private bool enableRectangle;
      private bool enableCircle;
        private bool enableTriangle;
            private bool enabletext1;
            private bool enabletext2;
            private bool enabletext3;
    private GameObject lastStroke;
private TrailRenderer tr;

GameObject thisTrail;
  GameObject thisCube;
  GameObject thisCircle;
  GameObject thisTriangle;
  GameObject thisText3;
  GameObject thisText2;
  GameObject thisText1;
  Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
           tr = trailPrefab.GetComponent<TrailRenderer>();
      //lastStroke = drawing.transform.GetChild(transform.childCount-1).gameObject;
       strokecounter = 2;
       enalbleTekenen = false;
       enableRectangle = false;
       enableCircle =false;
       enableTriangle = false;
       enabletext1 = false;
       enabletext2 = false;
       enabletext3 = false;
         tr.minVertexDistance = 0.1f;
          tr.material.SetTextureScale("_MainTex", new Vector2(5.0f,5.0f));
          upscaleX = new Vector3(1f,0,0);
           trailPrefab.GetComponent<Renderer>().material = colors[6];
        cubePrefab.GetComponent<Renderer>().material = colors[6];
        circlePrefab.GetComponent<Renderer>().material = colors[6];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[6];
        cubePointer.GetComponent<Renderer>().material = colors[6];
        circlePointer.GetComponent<Renderer>().material = colors[6];                //Beginkleur instellen
    }

    // Update is called once per frame
    void Update()
    {
        drawingcount =  drawing.transform.childCount-1;
         RaycastHit hit;
///////////////////////Teken logica, niet mogelijk om te tekeken wanneer eer een textobject of een shape geselecteerd is///////
         if (Input.GetKeyDown(KeyCode.Space) && enalbleTekenen == false && text3Pointer.activeSelf == false && text1Pointer.activeSelf == false && text2Pointer.activeSelf == false && cubePointer.activeSelf == false && circlePointer.activeSelf == false && trianglePointer.activeSelf==false)
        {
           
                thisTrail = (GameObject)Instantiate(trailPrefab, this.transform.position, Quaternion.identity);
           
           thisTrail.transform.SetParent(Camera.transform);
        
           enalbleTekenen = !enalbleTekenen;
            
         
          
           
        } else

        if(Input.GetKeyDown(KeyCode.Space) && enalbleTekenen == true){
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).SetParent(drawing.transform);
                       enalbleTekenen = !enalbleTekenen;
        }

      

       
    

      if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,1000)){
    
     Debug.DrawRay(transform.position, pointerRaycastPoint.transform.forward, Color.green);

     if(hit.collider.name.Contains("deleteStrokeCollider")){
               
                
                Destroy(drawing.transform.GetChild(drawingcount).gameObject);
            
                
            }

if (hit.collider.name.Contains("changePaintModeCollider")){  //Menu en logica om van scene te veranderen
    paintModeMenu.SetActive(true);
    Debug.Log("HIt change paint mode");

      }
      if (hit.collider.name.Contains("backPaintMode")){
    paintModeMenu.SetActive(false);

      }

      if (hit.collider.name.Contains("ontwerp")){
   SceneManager.LoadScene(3);

      }
       if (hit.collider.name.Contains("canvasTekenen")){
   SceneManager.LoadScene(2);

      }
      if (hit.collider.name.Contains("tekenen")){
   SceneManager.LoadScene(1);

      }
           /////////////Tools selecteren
     if(hit.collider.name.Contains("shapesDeselected")){
       shapesDeselected.SetActive(false);
       shapesSelected.SetActive(true);
       textSelected.SetActive(false);
       textDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
       shapesMenu.SetActive(true);
   
     }
if(shapesDeselected.activeSelf == true){
  shapesMenu.SetActive(false);
}
      if(hit.collider.name.Contains("gridDeselected")){
      
       gridDeselected.SetActive(false);
       gridSelected.SetActive(true);
       grids[0].SetActive(true);
       grids[1].SetActive(true);
       grids[2].SetActive(true);
       grids[3].SetActive(true);
       grids[4].SetActive(true);
       grids[5].SetActive(true);
     /*grids = GameObject.FindGameObjectsWithTag("grid");
     foreach(GameObject grid in grids){
       grid.SetActive(true);
     }*/
     }
     if(hit.collider.name.Contains("gridSelected")){
              gridDeselected.SetActive(true);
       gridSelected.SetActive(false);
       grids[0].SetActive(false);
       grids[1].SetActive(false);
       grids[2].SetActive(false);
       grids[3].SetActive(false);
       grids[4].SetActive(false);
       grids[5].SetActive(false);
     }

        if(hit.collider.name.Contains("penDeselected")){
       shapesDeselected.SetActive(true);
       shapesSelected.SetActive(false);
       textSelected.SetActive(false);
       textDeselected.SetActive(true);
       penDeselected.SetActive(false);
       penSelected.SetActive(true);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
       
         tr.startWidth = 2;
      tr.endWidth = 2;
      pointerRaycastPoint.transform.localScale = new Vector3(1f, 1f, 1f);
      tr.minVertexDistance = 25f;
     }


   if(hit.collider.name.Contains("textDeselected")){
       shapesDeselected.SetActive(true);
       shapesSelected.SetActive(false);
       textSelected.SetActive(true);
       textDeselected.SetActive(false);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
       
     
textMenu.SetActive(true);
     
      
   

     }

   if(hit.collider.name.Contains("rulerDeselected")){
       shapesDeselected.SetActive(true);
       shapesSelected.SetActive(false);
       textSelected.SetActive(false);
       textDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
       rulerDeselected.SetActive(false);
       rulerSelected.SetActive(true);
         
         tr.startWidth = 2;
      tr.endWidth = 2;
      pointerRaycastPoint.transform.localScale = new Vector3(1f, 1f, 1f);
      tr.minVertexDistance = 100;
     }

     //////////////////////Colors/////////////////

      if(hit.collider.name.Contains("redColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[0];
        cubePrefab.GetComponent<Renderer>().material = colors[0];
        circlePrefab.GetComponent<Renderer>().material = colors[0];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[0];
        cubePointer.GetComponent<Renderer>().material = colors[0];
        circlePointer.GetComponent<Renderer>().material = colors[0];
            }

      if(hit.collider.name.Contains("orangeColor")){
        trailPrefab.GetComponent<Renderer>().material = colors[1];
        cubePrefab.GetComponent<Renderer>().material = colors[1];
        circlePrefab.GetComponent<Renderer>().material = colors[1];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[1];
        cubePointer.GetComponent<Renderer>().material = colors[1];
        circlePointer.GetComponent<Renderer>().material = colors[1];

     }

       if(hit.collider.name.Contains("yellowColor")){
   trailPrefab.GetComponent<Renderer>().material = colors[2];
        cubePrefab.GetComponent<Renderer>().material = colors[2];
        circlePrefab.GetComponent<Renderer>().material = colors[2];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[2];
        cubePointer.GetComponent<Renderer>().material = colors[2];
        circlePointer.GetComponent<Renderer>().material = colors[2];

         
     }

      if(hit.collider.name.Contains("greenColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[3];
        cubePrefab.GetComponent<Renderer>().material = colors[3];
        circlePrefab.GetComponent<Renderer>().material = colors[3];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[3];
        cubePointer.GetComponent<Renderer>().material = colors[3];
        circlePointer.GetComponent<Renderer>().material = colors[3];

        }

       if(hit.collider.name.Contains("blueColor")){
 trailPrefab.GetComponent<Renderer>().material = colors[4];
        cubePrefab.GetComponent<Renderer>().material = colors[4];
        circlePrefab.GetComponent<Renderer>().material = colors[4];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[4];
        cubePointer.GetComponent<Renderer>().material = colors[4];
        circlePointer.GetComponent<Renderer>().material = colors[4];

         
     }

      if(hit.collider.name.Contains("purpleColor")){
    trailPrefab.GetComponent<Renderer>().material = colors[5];
        cubePrefab.GetComponent<Renderer>().material = colors[5];
        circlePrefab.GetComponent<Renderer>().material = colors[5];
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[5];
        cubePointer.GetComponent<Renderer>().material = colors[5];
        circlePointer.GetComponent<Renderer>().material = colors[5];

     }
     ////////////////////////////Textures///////////////////////////
     if(hit.collider.name.Contains("Ice1")){
         trailPrefab.GetComponent<Renderer>().material = textures[0];

       setTexturePref();
     }

      if(hit.collider.name.Contains("Ice2")){
         trailPrefab.GetComponent<Renderer>().material = textures[1];
setTexturePref();
     }

    if(hit.collider.name.Contains("flowerField")){
         trailPrefab.GetComponent<Renderer>().material = textures[2];

setTexturePref();
     }
 if(hit.collider.name.Contains("Grass")){
         trailPrefab.GetComponent<Renderer>().material = textures[3];

      setTexturePref();
     }
 if(hit.collider.name.Contains("sandRock")){
         trailPrefab.GetComponent<Renderer>().material = textures[4];
setTexturePref();
     }
      if(hit.collider.name.Contains("redRock")){
         trailPrefab.GetComponent<Renderer>().material = textures[5];
setTexturePref();
         
     }
 if(hit.collider.name.Contains("asphalt1")){
         trailPrefab.GetComponent<Renderer>().material = textures[6];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("asphalt2")){
         trailPrefab.GetComponent<Renderer>().material = textures[7];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("asphalt3")){
         trailPrefab.GetComponent<Renderer>().material = textures[8];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick1")){
         trailPrefab.GetComponent<Renderer>().material = textures[9];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick2")){
         trailPrefab.GetComponent<Renderer>().material = textures[10];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick3")){
         trailPrefab.GetComponent<Renderer>().material = textures[11];
setTexturePref();
         
     }
     /////////////////////////////////////////Canvas////////////////////////////
       if(hit.collider.name.Contains("A4P")){
         
      hideAllCanvas(0,1,2,3,4,5);
      disableCameras(0,1,2,3,4,5);
   
          }
        
     if(hit.collider.name.Contains("A4L")){
         hideAllCanvas(1,0,2,3,4,5);
         disableCameras(1,0,2,3,4,5);
     }
    
   
     if(hit.collider.name.Contains("A3C")){
         hideAllCanvas(2,0,1,3,4,5);
        disableCameras(2,0,1,3,4,5);
         
     }
     if(hit.collider.name.Contains("A2C")){
          hideAllCanvas(3,0,1,2,4,5);
        disableCameras(3,0,1,2,4,5);
         
     }
     if(hit.collider.name.Contains("business")){
         hideAllCanvas(4,0,1,2,3,5);
        disableCameras(4,0,1,2,3,5);
         
     }
     if(hit.collider.name.Contains("webpageC")){
         hideAllCanvas(5,0,1,2,3,4);
        disableCameras(5,0,1,2,3,4);
         
     }
////////////////////////////////////////////////Shapes///////////////// Prefab van de verschillende shapes gemaakt en deze instantieren wanneer er op spatie gedrukt wordt
if(hit.collider.name.Contains("cubeCollider") && enableRectangle == false){
  shapesMenu.SetActive(false);
  cubePointer.SetActive(true);

                thisCube = (GameObject)Instantiate(cubePrefab, this.transform.position, Quaternion.Euler(-90,0,0));
           
           thisCube.transform.SetParent(Camera.transform);
        thisCube.transform.eulerAngles = new Vector3(-90,0,0);
           enableRectangle = !enableRectangle;

}
 if(Input.GetKeyDown(KeyCode.Space) && enableRectangle == true){
    shapesMenu.SetActive(true);
  cubePointer.SetActive(false);
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).eulerAngles = new Vector3(-90,0,0);
           Camera.transform.GetChild(2).SetParent(drawing.transform);
               
                       enableRectangle = !enableRectangle;
                    
        }

if(hit.collider.name.Contains("circleCollider") && enableCircle == false){
  shapesMenu.SetActive(false);
  circlePointer.SetActive(true);
    thisCircle = (GameObject)Instantiate(circlePrefab, this.transform.position, Quaternion.Euler(90,0,0));
           
           thisCircle.transform.SetParent(Camera.transform);
        
           enableCircle = !enableCircle;
}

if(Input.GetKeyDown(KeyCode.Space) && enableCircle == true){
    shapesMenu.SetActive(true);
  circlePointer.SetActive(false);
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).SetParent(drawing.transform);
               
                       enableCircle = !enableCircle;
                    
        }
if(hit.collider.name.Contains("triangleCollider") && enableTriangle == false){
  shapesMenu.SetActive(false);
  trianglePointer.SetActive(true);
  thisTriangle = (GameObject)Instantiate(trianglePrefab, this.transform.position, Quaternion.Euler(0,0,0));
           
           thisTriangle.transform.SetParent(Camera.transform);
        
           enableTriangle= !enableTriangle;
}

if(Input.GetKeyDown(KeyCode.Space) && enableTriangle == true){
    shapesMenu.SetActive(true);
  trianglePointer.SetActive(false);
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           
          Camera.transform.GetChild(2).SetParent(drawing.transform);
               
                       enableTriangle = !enableTriangle;
                    
        }

        ////////////////////////////Scale shapes////////
        if(hit.collider.name.Contains("higherX")){
         
          drawing.transform.GetChild(drawingcount).gameObject.transform.localScale += new Vector3(0.2f,0,0);
          Debug.Log(drawingcount);
        }
        if(hit.collider.name.Contains("lowerX")){
         
          drawing.transform.GetChild(drawingcount).gameObject.transform.localScale -= new Vector3(0.2f,0,0);
        }

          if(hit.collider.name.Contains("higherY")){
          
          drawing.transform.GetChild(drawingcount).gameObject.transform.localScale += new Vector3(0,0,0.2f);
        }
        if(hit.collider.name.Contains("lowerY")){
         
          drawing.transform.GetChild(drawingcount).gameObject.transform.localScale -= new Vector3(0,0,0.2f);
        }
///////////////////Text///////////////// Prefab van de verschillende stukken tekst gemaakt en deze instantieren wanneer er op spatie gedrukt wordt

if(hit.collider.name.Contains("text3") && enabletext3 == false){
  textMenu.SetActive(false);
  text3Pointer.SetActive(true);

                thisText3 = (GameObject)Instantiate(text3Prefab, this.transform.position, Quaternion.Euler(0,0,0));
           
           thisText3.transform.SetParent(Camera.transform);
        thisText3.transform.eulerAngles = new Vector3(0,0,0);
           enabletext3 = !enabletext3;

}
 if(Input.GetKeyDown(KeyCode.Space) && enabletext1 == true){
    textMenu.SetActive(true);
  text3Pointer.SetActive(false);
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).eulerAngles = new Vector3(90,90,-90);
           Camera.transform.GetChild(2).SetParent(drawing.transform);
               
                       enabletext3 = !enabletext3;
                    
        }


        if(hit.collider.name.Contains("text2") && enabletext2 == false){
  textMenu.SetActive(false);
  text2Pointer.SetActive(true);

                thisText2 = (GameObject)Instantiate(text2Prefab, this.transform.position, Quaternion.Euler(0,0,0));
           
           thisText2.transform.SetParent(Camera.transform);
        thisText2.transform.eulerAngles = new Vector3(0,0,0);
           enabletext2 = !enabletext2;

}
 if(Input.GetKeyDown(KeyCode.Space) && enabletext2 == true){
    textMenu.SetActive(true);
  text2Pointer.SetActive(false);
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).eulerAngles = new Vector3(90,90,-90);
           Camera.transform.GetChild(2).SetParent(drawing.transform);
               
                       enabletext2 = !enabletext2;
                    
        }


        if(hit.collider.name.Contains("text1") && enabletext1 == false){
  textMenu.SetActive(false);
  text1Pointer.SetActive(true);

                thisText1 = (GameObject)Instantiate(text1Prefab, this.transform.position, Quaternion.Euler(0,0,0));
           
           thisText1.transform.SetParent(Camera.transform);
        thisText1.transform.eulerAngles = new Vector3(0,0,0);
           enabletext1 = !enabletext1;

}
 if(Input.GetKeyDown(KeyCode.Space) && enabletext1 == true){
    textMenu.SetActive(true);
  text1Pointer.SetActive(false);
           Debug.Log(Camera.transform.GetChild(strokecounter-1).name);
           Camera.transform.GetChild(2).eulerAngles = new Vector3(90,90,-90);
           Camera.transform.GetChild(2).SetParent(drawing.transform);
               
                       enabletext1 = !enabletext1;
                    
        }



/////////////////////////menu's/////////////////
      if(hit.collider.name.Contains("colorSelection")){
     ColorsUi.SetActive(true);
   Textures.SetActive(false);
   
     selections.SetActive(false);
     shapesMenu.SetActive(false);
            }

            if(hit.collider.name.Contains("natureSelection")){
              Textures.SetActive(true);
     ColorsUi.SetActive(false);
   ToolsUi.SetActive(false);
    selections.SetActive(false);       
    shapesMenu.SetActive(false); 
      }


            if(hit.collider.name.Contains("settingSelection")){
     ColorsUi.SetActive(false);
   Textures.SetActive(false);
selections.SetActive(false);
ToolsUi.SetActive(false);
settings.SetActive(true);
shapesMenu.SetActive(false);
            }
               if(hit.collider.name.Contains("changeSettingsCollider")){
     
settings.SetActive(false);
canvasMenu.SetActive(true);
disableAllCanvas();
            }

            if(hit.collider.name.Contains("changeBgCollider")){
     
settings.SetActive(false);
canvasMenu.SetActive(false);
disableAllCanvas();
bgcanvases.SetActive(true);

if(canvases[0].activeSelf == true){
                   showBackgroundMenu(1,0,2,3,4);
               }
                if(canvases[1].activeSelf == true){
                   showBackgroundMenu(0,1,2,3,4);
                   Debug.Log("ACTIEF");
               }
                 if(canvases[2].activeSelf){
                   showBackgroundMenu(2,0,1,3,4);
               }
                if(canvases[3].activeSelf){
                   showBackgroundMenu(2,0,1,3,4);
               }
                if(canvases[4].activeSelf){
                   showBackgroundMenu(3,0,1,2,4);
               }
                if(canvases[5].activeSelf){
                   showBackgroundMenu(4,0,1,2,3);
               }
            }
/////////////////////////////////////////// Background van de canvases////////////////////////////
            if(hit.collider.name.Contains("White")){
              setWhiteBackground();
            }
            if(hit.collider.name.Contains("Black")){
              setBlackBackground();
            }
            if(hit.collider.name.Contains("A4LDarkGradient")){
              canvases[1].GetComponent<Renderer>().material = Backgrounds[2];
            }
            if(hit.collider.name.Contains("A4LGradient")){
              canvases[1].GetComponent<Renderer>().material = Backgrounds[3];
            }
            if(hit.collider.name.Contains("A4LAbstract")){
              canvases[1].GetComponent<Renderer>().material = Backgrounds[4];
            }
            if(hit.collider.name.Contains("A4PGradient")){
              canvases[0].GetComponent<Renderer>().material = Backgrounds[5];
            }
            if(hit.collider.name.Contains("A4PDarkGradient")){
              canvases[0].GetComponent<Renderer>().material = Backgrounds[2];
            }
            if(hit.collider.name.Contains("A4PAbstract")){
              canvases[0].GetComponent<Renderer>().material = Backgrounds[4];
            }
            if(hit.collider.name.Contains("A3CAbstract1")){
            canvases[2].GetComponent<Renderer>().material = Backgrounds[6];
            canvases[3].GetComponent<Renderer>().material = Backgrounds[6];
            }
            if(hit.collider.name.Contains("A3CAbstract2")){ 
            canvases[2].GetComponent<Renderer>().material = Backgrounds[7];
            canvases[3].GetComponent<Renderer>().material = Backgrounds[7];
            }
            if(hit.collider.name.Contains("A3CAbstract3")){ 
            canvases[2].GetComponent<Renderer>().material = Backgrounds[4];
            canvases[3].GetComponent<Renderer>().material = Backgrounds[4];
            }
            if(hit.collider.name.Contains("buisnessGradient")){
              canvases[4].GetComponent<Renderer>().material = Backgrounds[5];
            }
            if(hit.collider.name.Contains("buisnessAbstract")){
              canvases[4].GetComponent<Renderer>().material = Backgrounds[8];
            }
            if(hit.collider.name.Contains("buisnessAbstract2")){
              canvases[4].GetComponent<Renderer>().material = Backgrounds[9];
            }
            if(hit.collider.name.Contains("buisnessAbstract2")){
              canvases[4].GetComponent<Renderer>().material = Backgrounds[9];
            }

            if(hit.collider.name.Contains("webpageBg1")){
              canvases[5].GetComponent<Renderer>().material = Backgrounds[10];
            }

            if(hit.collider.name.Contains("webpageBg2")){
              canvases[5].GetComponent<Renderer>().material = Backgrounds[11];
            }
            if(hit.collider.name.Contains("webpageGradient")){
              canvases[5].GetComponent<Renderer>().material = Backgrounds[12];
            }

/////////////////////////////////////////// Terug knoppen////////////////////////////
             if(hit.collider.name.Contains("backCol")){
     ColorsUi.SetActive(false);
  selections.SetActive(true);
     ToolsUi.SetActive(true);
            }
            

                if(hit.collider.name.Contains("backNat")){
     Textures.SetActive(false);
    selections.SetActive(true);
     ToolsUi.SetActive(true);
            }
    if(hit.collider.name.Contains("backSet")){
    settings.SetActive(false);
    selections.SetActive(true);
     ToolsUi.SetActive(true);
     enableAllCanvas();
            }
     if(hit.collider.name.Contains("backCanvas")){
    settings.SetActive(true);
   canvasMenu.SetActive(false);
    enableAllCanvas();
            }

            if(hit.collider.name.Contains("backBackground")){
              settings.SetActive(true);
canvasMenu.SetActive(false);
enableAllCanvas();
bgcanvases.SetActive(false);
            }
   if(hit.collider.name.Contains("backText")){
             textMenu.SetActive(false);
            }

    }
     
}
 public void setColorPref(){
  tr.textureMode = LineTextureMode.Stretch;
}
    public void setTexturePref(){
tr.minVertexDistance = 30f;
         tr.numCornerVertices = 10;
              tr.startWidth = 20;
      tr.endWidth = 20;
      tr.textureMode = LineTextureMode.RepeatPerSegment;
    }

    //////////////////Functies die alle cnavassen gaat hidden en de juiste selecteren.
    public void hideAllCanvas(int true1, int false1,int false2,int false3,int false4,int false5){ //Het geselecteerde canvas tonen
       canvases[true1].SetActive(true);
       canvases[false1].SetActive(false);
       canvases[false2].SetActive(false);
       canvases[false3].SetActive(false);
       canvases[false4].SetActive(false);
       canvases[false5].SetActive(false);
    }
      public void disableCameras(int true1, int false1,int false2,int false3,int false4,int false5){ //De camera met dezelfde resolutie als het canvas actief zetten
       cameras[true1].gameObject.SetActive(true);
       cameras[false1].gameObject.SetActive(false);
       cameras[false2].gameObject.SetActive(false);
       cameras[false3].gameObject.SetActive(false);
       cameras[false4].gameObject.SetActive(false);
       cameras[false5].gameObject.SetActive(false);
    }
    public void disableAllCanvas(){ //alle canvassen onzichtbaar zetten
      canvases[0].GetComponent<Renderer>().enabled = false;
      canvases[1].GetComponent<Renderer>().enabled = false;
      canvases[2].GetComponent<Renderer>().enabled = false;
      canvases[3].GetComponent<Renderer>().enabled = false;
      canvases[4].GetComponent<Renderer>().enabled = false;
      canvases[5].GetComponent<Renderer>().enabled = false;
    }

     public void enableAllCanvas(){
      canvases[0].GetComponent<Renderer>().enabled = true;
      canvases[1].GetComponent<Renderer>().enabled = true;
      canvases[2].GetComponent<Renderer>().enabled = true;
      canvases[3].GetComponent<Renderer>().enabled = true;
      canvases[4].GetComponent<Renderer>().enabled = true;
      canvases[5].GetComponent<Renderer>().enabled = true;
    }

      public void showBackgroundMenu(int true1, int false1,int false2,int false3,int false4){
       canvasBackgrounds[true1].SetActive(true);
       canvasBackgrounds[false1].SetActive(false);
       canvasBackgrounds[false2].SetActive(false);
       canvasBackgrounds[false3].SetActive(false);
       canvasBackgrounds[false4].SetActive(false);
   
    }
    public void setWhiteBackground(){
      canvases[0].GetComponent<Renderer>().material = Backgrounds[0];
      canvases[1].GetComponent<Renderer>().material = Backgrounds[0];
      canvases[2].GetComponent<Renderer>().material = Backgrounds[0];
      canvases[3].GetComponent<Renderer>().material = Backgrounds[0];
      canvases[4].GetComponent<Renderer>().material = Backgrounds[0];
      canvases[5].GetComponent<Renderer>().material = Backgrounds[0];
    }
        public void setBlackBackground(){
      canvases[0].GetComponent<Renderer>().material = Backgrounds[1];
      canvases[1].GetComponent<Renderer>().material = Backgrounds[1];
      canvases[2].GetComponent<Renderer>().material = Backgrounds[1];
      canvases[3].GetComponent<Renderer>().material = Backgrounds[1];
      canvases[4].GetComponent<Renderer>().material = Backgrounds[1];
      canvases[5].GetComponent<Renderer>().material = Backgrounds[1];
    }
}

