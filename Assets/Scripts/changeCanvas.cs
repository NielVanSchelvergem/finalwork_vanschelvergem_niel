﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeCanvas : MonoBehaviour
{
    public GameObject pointer;
    public GameObject pointerRaycastPoint;

     public Material[] myMaterials = new Material[5];
     public GameObject canvas;
     public GameObject canvasMenu;
     public GameObject paintmodeMenu;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      RaycastHit hit;

        if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,100)){

         
           
            if(hit.collider.name.Contains("changeSettingsCollider")){
          canvasMenu.SetActive(true);
          paintmodeMenu.SetActive(false);
            }
              if(hit.collider.name.Contains("backCanvas")){
          canvasMenu.SetActive(false);
            }
            if(hit.collider.name.Contains("cnv1")){
                canvas.GetComponent<Renderer>().material = myMaterials[0];
            }
             if(hit.collider.name.Contains("cnv2")){
                canvas.GetComponent<Renderer>().material = myMaterials[1];
            }
             if(hit.collider.name.Contains("cnv3")){
                canvas.GetComponent<Renderer>().material = myMaterials[2];
            }
             if(hit.collider.name.Contains("cnv4")){
        canvas.GetComponent<Renderer>().material = myMaterials[3];
            }
             if(hit.collider.name.Contains("cnv5")){
                canvas.GetComponent<Renderer>().material = myMaterials[4];
            }

            
        }
    
   
    }
}
