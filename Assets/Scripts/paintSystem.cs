﻿
/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paintSystem : MonoBehaviour
{
      public  GameObject pointer;
    public GameObject pointerRaycastPoint;
  
  public GameObject trailPrefab;
  public GameObject pencilPrefab;
  GameObject thisTrail;
  Vector3 startPos;

  public Plane tekenCanvas;
   
    public GameObject Camera;
    public GameObject Player;
    public GameObject drawing;
    private int strokecounter;
    private bool enalbleTekenen;
    private GameObject lastStroke;

    public Material[] colors = new Material[7];
        

    public Material[] textures = new Material[12];
     public Material[] effectsMovable = new Material[6];
      public Material[] effectsNonMovable = new Material[6];
    public GameObject brushSelected;
    public GameObject brushDeselected;
    public GameObject penSelected;
    public GameObject penDeselected;
    public GameObject pencilSelected;
    public GameObject pencilDeselected;

        public GameObject rulerSelected;
    public GameObject rulerDeselected;
            public GameObject rollerSelected;
    public GameObject rollerDeselected;
      


      
        public GameObject ColorsUi;
        public GameObject ToolsUi;
        public GameObject selections;
      
        public GameObject Textures;
        public GameObject effects;
        
private TrailRenderer tr;

    
    

//bool TouchButton = GvrControllerInput.ClickButton;

    void Start()
    {
      tr = trailPrefab.GetComponent<TrailRenderer>();
       
       strokecounter = 2;
       enalbleTekenen = false;
         tr.minVertexDistance = 0.1f;
          tr.material.SetTextureScale("_MainTex", new Vector2(5.0f,5.0f));
          
     
    }

    // Update is called once per frame
    void Update()  
    {
         RaycastHit hit;

         if (Input.GetKeyDown(KeyCode.Space) && enalbleTekenen == false)  //logica tekeken met meerdere lijnen
        {
           
           {
                thisTrail = (GameObject)Instantiate(trailPrefab, this.transform.position, Quaternion.identity);
           
           thisTrail.transform.SetParent(Camera.transform);
        
           enalbleTekenen = !enalbleTekenen;
            }
         
          
           
        } else

        if(Input.GetKeyDown(KeyCode.Space) && enalbleTekenen == true){
          
           
           Camera.transform.GetChild(2).SetParent(drawing.transform);
                       enalbleTekenen = !enalbleTekenen;
        }
        

      if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,1000000)){ //raycast loodrecht op de pointer
     
     Debug.DrawRay(transform.position, pointerRaycastPoint.transform.forward, Color.magenta,100000f);
////////////// Selecteren tools//////////////
     if(hit.collider.name.Contains("brushDeselected")){
       brushDeselected.SetActive(false);
       brushSelected.SetActive(true);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
        rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
      tr.startWidth = 10;
      tr.endWidth = 10;
      pointerRaycastPoint.transform.localScale = new Vector3(10f, 10f, 10f);
      tr.minVertexDistance = 0.1f;
     }

      if(hit.collider.name.Contains("rollerDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
       rollerDeselected.SetActive(false);
       rollerSelected.SetActive(true);
      tr.startWidth = 30;
      tr.endWidth = 30;
      pointerRaycastPoint.transform.localScale = new Vector3(30f, 30f, 30f);
      tr.minVertexDistance = 0.1f;
     }

        if(hit.collider.name.Contains("penDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(false);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(false);
       penSelected.SetActive(true);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
          rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
         tr.startWidth = 1;
      tr.endWidth = 1;
      pointerRaycastPoint.transform.localScale = new Vector3(1f, 1f, 1f);
      tr.minVertexDistance = 0.1f;
     }


   if(hit.collider.name.Contains("pencilDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(true);
       pencilDeselected.SetActive(false);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
        rulerDeselected.SetActive(true);
       rulerSelected.SetActive(false);
          rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
        tr.startWidth = 3;
     tr.endWidth = 3;
      pointerRaycastPoint.transform.localScale = new Vector3(3f, 3f, 3f);
      tr.minVertexDistance = 0.1f;
      //trailPrefab.GetComponent<Renderer>().material= colors[0];
     
      
   

     }

   if(hit.collider.name.Contains("rulerDeselected")){
       brushDeselected.SetActive(true);
       brushSelected.SetActive(false);
       pencilSelected.SetActive(true);
       pencilDeselected.SetActive(true);
       penDeselected.SetActive(true);
       penSelected.SetActive(false);
       rulerDeselected.SetActive(false);
       rulerSelected.SetActive(true);
          rollerDeselected.SetActive(true);
       rollerSelected.SetActive(false);
         tr.startWidth = 5;
      tr.endWidth = 5;
      pointerRaycastPoint.transform.localScale = new Vector3(5f, 5f, 5f);
      tr.minVertexDistance = 100;
     }

     //////////////////////Colors/////////////////

      if(hit.collider.name.Contains("redColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[0];
        
        pointerRaycastPoint.GetComponent<Renderer>().material = colors[0];
            }

      if(hit.collider.name.Contains("orangeColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[1];
        

         pointerRaycastPoint.GetComponent<Renderer>().material = colors[1];
     }

       if(hit.collider.name.Contains("yellowColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[2];
        
         pointerRaycastPoint.GetComponent<Renderer>().material = colors[2];
         
     }

      if(hit.collider.name.Contains("greenColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[3];
        
         pointerRaycastPoint.GetComponent<Renderer>().material = colors[3];
     }

       if(hit.collider.name.Contains("blueColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[4];
       
         pointerRaycastPoint.GetComponent<Renderer>().material = colors[4];
         
     }

      if(hit.collider.name.Contains("purpleColor")){
         trailPrefab.GetComponent<Renderer>().material = colors[5];
         pointerRaycastPoint.GetComponent<Renderer>().material =colors[5];
     }

 
     ////////////////////////////Textures///////////////////////////
     if(hit.collider.name.Contains("Ice1")){
         trailPrefab.GetComponent<Renderer>().material = textures[0];

       setTexturePref();
     }

      if(hit.collider.name.Contains("Ice2")){
         trailPrefab.GetComponent<Renderer>().material = textures[1];
setTexturePref();
     }

    if(hit.collider.name.Contains("flowerField")){
         trailPrefab.GetComponent<Renderer>().material = textures[2];

setTexturePref();
     }
 if(hit.collider.name.Contains("Grass")){
         trailPrefab.GetComponent<Renderer>().material = textures[3];

      setTexturePref();
     }
 if(hit.collider.name.Contains("sandRock")){
         trailPrefab.GetComponent<Renderer>().material = textures[4];
setTexturePref();
     }
      if(hit.collider.name.Contains("redRock")){
         trailPrefab.GetComponent<Renderer>().material = textures[5];
setTexturePref();
         
     }
 if(hit.collider.name.Contains("asphalt1")){
         trailPrefab.GetComponent<Renderer>().material = textures[6];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("asphalt2")){
         trailPrefab.GetComponent<Renderer>().material = textures[7];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("asphalt3")){
         trailPrefab.GetComponent<Renderer>().material = textures[8];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick1")){
         trailPrefab.GetComponent<Renderer>().material = textures[9];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick2")){
         trailPrefab.GetComponent<Renderer>().material = textures[10];
setTexturePref();
         
     }
      if(hit.collider.name.Contains("brick3")){
         trailPrefab.GetComponent<Renderer>().material = textures[11];
setTexturePref();
         
     }
////////////////////////////////////////Effects Movable/////////////////////////////////////
 if(hit.collider.name.Contains("orangeLineM")){
         trailPrefab.GetComponent<Renderer>().material = effectsMovable[0];
         
setEffectPref();
     }
     if(hit.collider.name.Contains("moveSparkle")){
      trailPrefab.GetComponent<Renderer>().material = effectsMovable[1];
      setEffectPref();
     }
     if(hit.collider.name.Contains("moveDarkMatter")){
         trailPrefab.GetComponent<Renderer>().material = effectsMovable[2];
setEffectPref();
     }
     if(hit.collider.name.Contains("moveRedLines")){
        setEffectPref();
        
         trailPrefab.GetComponent<Renderer>().material = effectsMovable[3];

     }
     if(hit.collider.name.Contains("moveBlueWave")){
         trailPrefab.GetComponent<Renderer>().material = effectsMovable[4];
setEffectPref();
     }
     if(hit.collider.name.Contains("movePlazma")){
         trailPrefab.GetComponent<Renderer>().material = effectsMovable[5];
setEffectPref();
     }
////////////////////////////////////////Effects NoN Movable/////////////////////////////////////

      if(hit.collider.name.Contains("orangeLines")){
         trailPrefab.GetComponent<Renderer>().material = effectsNonMovable[0];
setEffectPref();
     }
     if(hit.collider.name.Contains("sparkle")){
         trailPrefab.GetComponent<Renderer>().material = effectsNonMovable[1];
setEffectPref();
     }
     if(hit.collider.name.Contains("darkMatter")){
         trailPrefab.GetComponent<Renderer>().material = effectsNonMovable[2];
setEffectPref();
     }
     if(hit.collider.name.Contains("redLines")){
         trailPrefab.GetComponent<Renderer>().material = effectsNonMovable[3];
setEffectPref();
     }
     if(hit.collider.name.Contains("blueWave")){
         trailPrefab.GetComponent<Renderer>().material = effectsNonMovable[4];
setEffectPref();
     }
     if(hit.collider.name.Contains("plazma")){
         trailPrefab.GetComponent<Renderer>().material = effectsNonMovable[5];
setEffectPref();
     }
///////////////////////Menu's///////////////////

      if(hit.collider.name.Contains("colorSelection")){
     ColorsUi.SetActive(true);
   Textures.SetActive(false);
   effects.SetActive(false);
     selections.SetActive(false);
            }

            if(hit.collider.name.Contains("natureSelection")){
              Textures.SetActive(true);
     ColorsUi.SetActive(false);
   ToolsUi.SetActive(false);
    selections.SetActive(false);          }

            if(hit.collider.name.Contains("effectSelection")){
     ColorsUi.SetActive(false);
   Textures.SetActive(false);
selections.SetActive(false);
effects.SetActive(true);
ToolsUi.SetActive(false);
            }

             if(hit.collider.name.Contains("backCol")){
     ColorsUi.SetActive(false);
  selections.SetActive(true);
     ToolsUi.SetActive(true);
            }

                if(hit.collider.name.Contains("backNat")){
     Textures.SetActive(false);
    selections.SetActive(true);
     ToolsUi.SetActive(true);
            }
    if(hit.collider.name.Contains("backEff")){
     effects.SetActive(false);
    selections.SetActive(true);
     ToolsUi.SetActive(true);
            }

  
        }   
    }

    //De instellingen voor het tekenobject met trailrenderer goedzetten voor kleuren,textures en effecten
public void setColorPref(){
  tr.textureMode = LineTextureMode.Stretch;
}
    public void setTexturePref(){
tr.minVertexDistance = 50f;
         tr.numCornerVertices = 10;
              tr.startWidth = 40;
      tr.endWidth = 40;
      tr.textureMode = LineTextureMode.RepeatPerSegment;
    }

    public void setEffectPref(){
   tr.startWidth = 10;
      tr.endWidth = 10;
      tr.textureMode = LineTextureMode.Stretch;
      
}
}   

