﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Board to Bits Games (2018) Geraadpleegd op 10 juni 2020 https://www.youtube.com/watch?v=d-56p770t0U&t=1548s; Tutorial foto nemen met camera.

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Camera))]
public class takePicture2 : MonoBehaviour
{
    public Camera A4PCamera;
    int resWidth = 256;
    int resHeight=265;
    public GameObject Pointer;
    public GameObject pointerRaycastPoint;
    private bool Takepic;
    public GameObject settings;
    // Start is called before the first frame update
    void Start()
    {
     A4PCamera = GetComponent<Camera>();  
     if(A4PCamera.targetTexture == null){
         A4PCamera.targetTexture = new RenderTexture(resWidth,resHeight,24);
     } else{
         resWidth = A4PCamera.targetTexture.width;
         resHeight = A4PCamera.targetTexture.height;
     }
     Takepic = false;
     
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,1000)){

           Debug.DrawRay(transform.position, pointerRaycastPoint.transform.forward, Color.green); //wanneer we met de pointer een over het fotneem icoon hoveren gaan we een foto nemen
          if(hit.collider.name.Contains("takePicCollider")){
              Debug.Log("collider wroks");
settings.SetActive(false);
Pointer.SetActive(false);
          callTakeSnapshot();
         // settings.SetActive(true);
          }

            if(Takepic){
                
            Texture2D snapshot = new Texture2D(resWidth,resHeight,TextureFormat.RGB24,false); // Zet de resolutie naar die van de door mij gemaakte target texture
            A4PCamera.Render();
            RenderTexture.active = A4PCamera.targetTexture;
            snapshot.ReadPixels(new Rect(0,0,resWidth,resHeight),0,0);
            byte[] bytes = snapshot.EncodeToPNG();
            string filename = SnapshotName();
            System.IO.File.WriteAllBytes(filename,bytes);
            Debug.Log("Picture taken");
            settings.SetActive(true);
            Pointer.SetActive(true);
            Takepic = false;
        }
        }
    }
    public void callTakeSnapshot(){
        Takepic = true;
    }
    
    
        
        string SnapshotName(){
            return string.Format("{0}/Snapshots/snap_{1}x{2}_{3}.png", Application.dataPath, resWidth,resHeight,System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        }
}
