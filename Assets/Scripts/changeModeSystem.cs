﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class changeModeSystem : MonoBehaviour
{
    public  GameObject pointer;
    public GameObject pointerRaycastPoint;
    public GameObject paintModeMenu;
    public GameObject backgroundMenu;

    void Start()
    {
       
         
 

    }

    // Update is called once per frame
    void Update()
    {
          RaycastHit hit;

        if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,100)){
            Debug.DrawRay(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward*1000f, Color.magenta);
if (hit.collider.name.Contains("changePaintModeCollider")){
    paintModeMenu.SetActive(true);
backgroundMenu.SetActive(false);
    

      }
      if (hit.collider.name.Contains("backPaintMode")){
    paintModeMenu.SetActive(false);

      }

      if (hit.collider.name.Contains("ontwerp")){
   SceneManager.LoadScene(3);

      }
       if (hit.collider.name.Contains("canvasTekenen")){
   SceneManager.LoadScene(2);

      }
      if (hit.collider.name.Contains("tekenen")){
   SceneManager.LoadScene(1);

      }



        }   
        
    }
}
