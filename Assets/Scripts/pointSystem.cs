﻿/*
Auteur Van Schelvergem Niel
Final Work Erasmushogeschool Brussel - Multimedia en communicatietechnologie
2019-2020

bronnen:


Zenva.(2017) tGeraadpleegd op 25 maart 2020   https://www.youtube.com/watch?v=eaqwszsH6Jg&t

Holisitc(2016) tGeraadpleegd op 29 april 2020 https://www.youtube.com/watch?v=xlwuGKTyJBs&tt

Unity documentatie: https://docs.unity3d.com/Manual/index.html

Unity forum: https://forum.unity.com/forums/documentation.59/

Stackoverflow: https://stackoverflow.com/

Peter Dickx: Slides Real-time 3D²


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pointSystem : MonoBehaviour
{
    public static GameObject pointer;
    public  GameObject introHoofdSelected;
    public  GameObject introHoofdDeselected;

    public  GameObject introControllerSelected;

    public  GameObject introControllerDeselected;
    private GameObject pointerRaycastPoint;

    public GameObject introCanvas;
        public GameObject menuCanvas;
    public GameObject menuLButtonSelected;
    public GameObject menuRbuttonSelected;
 public GameObject menuLButtonDeselected;
    public GameObject menuRbuttonDeselected;

     public GameObject tekenKeuze;
    public GameObject skiKeuze;
public Material actifIndi;
public Material passifIndi;

public GameObject tekenenIndi;
public GameObject skienIndi;
    

    public float huidigeTijd = 0f;  //De huidige tijd van onze aftelklok bijhouden
    public float startTijd= 3f;  //De startijd van onze aftelklok bijhouden
    public Text timer;            //De "tekst" die we tonen (=de timer zelf) 
    // Start is called before the first frame update
    // Start is called before the first frame update
    void Start()
    {
      pointer = gameObject.transform.GetChild(0).gameObject;
        pointerRaycastPoint =  pointer.transform.GetChild(0).gameObject;
       huidigeTijd = startTijd;  //We zetten de huidige tijd gelijk aan de starttijd   
    }

    // Update is called once per frame
    void Update()
    {
                RaycastHit hit;

        if(Physics.Raycast(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, out hit,1000)){
            
            Debug.DrawRay(pointerRaycastPoint.transform.position, pointerRaycastPoint.transform.forward, Color.magenta);
           
  //Wanneer dat we op het wapen aan de muur of op de desk mikken, zal dat gameobject zichtbaar worden en de andere niet zichtbaar
    if (hit.collider.name.Contains("HoofdCollider")){
    
    huidigeTijd -= 1* Time.deltaTime;    //Elke frame gaan we de overlopentijd van die frame van onze afteltijd doen(Elke seconde gaat er een seconde van de klok)
    timer.text = huidigeTijd.ToString("0");  //We plaatsen het aftellen van de tijd in een tekstvak (De tijd is een int dus moeten we een toString doen, de 0 zorgt ervoor dat we geen cijfers na de komma hebben )


introHoofdDeselected.SetActive(false);
introHoofdSelected.SetActive(true);
introControllerDeselected.SetActive(true);
introControllerSelected.SetActive(false);

      }else
           if (hit.collider.name.Contains("ConCollider")){
                huidigeTijd -= 1* Time.deltaTime;    //Elke frame gaan we de overlopentijd van die frame van onze afteltijd doen(Elke seconde gaat er een seconde van de klok)
    timer.text = huidigeTijd.ToString("0");  //We plaatsen het aftellen van de tijd in een tekstvak (De tijd is een int dus moeten we een toString doen, de 0 zorgt ervoor dat we geen cijfers na de komma hebben )
introControllerDeselected.SetActive(false);
introControllerSelected.SetActive(true);
introHoofdDeselected.SetActive(true);
introHoofdSelected.SetActive(false);
      } /*else {
          
          introHoofdDeselected.SetActive(true);
          introHoofdSelected.SetActive(false);
          introControllerDeselected.SetActive(true);
          introControllerSelected.SetActive(false);
      }*/

      if(hit.collider.name.Contains("menuButtonLDeselected")){
          menuLButtonDeselected.SetActive(false);
          menuLButtonSelected.SetActive(true);
          menuRbuttonDeselected.SetActive(true);
          menuRbuttonSelected.SetActive(false);
   skiKeuze.SetActive(true);
 

      } else if(hit.collider.name.Contains("menuButtonRDeselected")){
 menuRbuttonDeselected.SetActive(false);
          menuRbuttonSelected.SetActive(true);
           menuLButtonDeselected.SetActive(true);
          menuLButtonSelected.SetActive(false);
          
   
      }

       if(skiKeuze.activeSelf && skiKeuze.transform.position.x >= -50){
          skiKeuze.transform.Translate(-300*Time.deltaTime,0f,0f); 
          tekenKeuze.transform.Translate(-300*Time.deltaTime,0f,0f); 
       } 
if(skiKeuze.activeSelf && skiKeuze.transform.position.x < -50){
           tekenKeuze.SetActive(false);
           skienIndi.GetComponent<Renderer>().sharedMaterial = actifIndi;
           tekenenIndi.GetComponent<Renderer>().sharedMaterial = passifIndi;
       } 



     if(huidigeTijd <= 0){
 menuCanvas.SetActive(true);
 menuCanvas.transform.Translate(0f,0f,-400*Time.deltaTime); 
 introCanvas.transform.Translate(0f,0f,-200*Time.deltaTime);  
}
    if(menuCanvas.transform.position.z <= -80){
     huidigeTijd =3f;
     introCanvas.SetActive(false);
 }     
     }    
    }
}
